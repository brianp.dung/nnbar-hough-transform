#include <HoughTransform.h>

int main() {
    std::vector<point2d_t> points;
    points.push_back(std::make_pair(1., 1.));
    points.push_back(std::make_pair(2., 2.));
    points.push_back(std::make_pair(3., 3.));
    points.push_back(std::make_pair(4., 4.));
    points.push_back(std::make_pair(5., 5.));

    points.push_back(std::make_pair(1.+0.2, 5.));
    points.push_back(std::make_pair(2.+0.2, 4.));
    points.push_back(std::make_pair(3.+0.2, 3.));
    points.push_back(std::make_pair(4.+0.2, 2.));
    points.push_back(std::make_pair(5.+0.2, 1.));

    HoughTransform* ht = new HoughTransform(points);
    ht->SetAngleResolution(TMath::Pi() / 1000);
    ht->SetHoughThreshold(2);
    ht->Transform();
    ht->Plot();

    return 0;
}
