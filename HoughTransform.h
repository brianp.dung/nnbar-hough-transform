#pragma once

#include <utility>
#include <vector>
#include <map>
#include <tuple>
#include <iostream>

#include <TH2.h>
#include <TMath.h>
#include <TGraph.h>
#include <TStyle.h>
#include <TCanvas.h>
#include <TMultiGraph.h>

typedef std::pair<double, double> point2d_t;
typedef std::tuple<double, double, double> line2d_t;

class HoughTransform {
public:
    HoughTransform(const std::vector<point2d_t>& hits);
    virtual ~HoughTransform();

    void SetAngleResolution(double angleResolution);
    void SetHoughThreshold(size_t nlines);
    void Transform();
    void Plot();

    std::vector<point2d_t> GetLines() const;
    std::map<size_t, std::vector<point2d_t>>GetAssortedLines() const;

    std::vector<point2d_t> GetHits() const;
    std::map<size_t, std::vector<point2d_t>> GetAssortedHits() const;

private:
    double _AngleResolution;

    size_t _NHits;

    std::vector<point2d_t> _LineParameterSpace;
    TH2D* _ParameterDensity;

    std::vector<point2d_t> _Hits;
    std::vector<point2d_t> _Lines;
    size_t _NLines;

    std::map<size_t, std::vector<point2d_t>> _AssortedLines;
    std::map<size_t, std::vector<point2d_t>> _AssortedHits;

private:
    void PointTransform(point2d_t point);
    double DistanceFromPointToLine(point2d_t point, line2d_t line);
    void FormLines();
};
