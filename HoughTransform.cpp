#include "HoughTransform.h"

HoughTransform::HoughTransform(const std::vector<point2d_t>& hits) : _AngleResolution(TMath::Pi()/1000),
                                                                     _NLines(4) {
    for (auto & hit : hits) {
        _Hits.push_back(hit);
    }
    _LineParameterSpace.clear();
    _NHits = _Hits.size();
}

HoughTransform::~HoughTransform() = default;

void HoughTransform::SetAngleResolution(double angleResolution) {
    _AngleResolution = angleResolution;
}

std::vector<point2d_t> HoughTransform::GetLines() const {
    return _Lines;
}

std::map<size_t, std::vector<point2d_t>> HoughTransform::GetAssortedLines() const {
    return _AssortedLines;
}

std::vector<point2d_t> HoughTransform::GetHits() const {
    return _Hits;
}

std::map<size_t, std::vector<point2d_t>> HoughTransform::GetAssortedHits() const {
    return _AssortedHits;
}

void HoughTransform::Transform() {
    for (auto & _Hit : _Hits) {
        PointTransform(_Hit);
    }
    FormLines();
}

void HoughTransform::FormLines() {
    double xllim = 0;
    double xulim = TMath::Pi() - _AngleResolution;
    double yllim = +1E99;
    double yulim = -1E99;
    for (auto & _Line : _LineParameterSpace) {
        yllim = yllim < _Line.second ? yllim : _Line.second;
        yulim = yulim > _Line.second ? yulim : _Line.second;
    }

    _ParameterDensity = new TH2D("", "", 500, xllim, xulim, 500, yllim, yulim);
    for (auto & _Line : _LineParameterSpace) {
        _ParameterDensity->Fill(_Line.first, _Line.second);
    }

    for (size_t i = 0; i < _NLines; i++) {
        Int_t locmaxx;
        Int_t locmaxy;
        Int_t locmaxz;
        Int_t binmax = _ParameterDensity->GetMaximumBin(locmaxx, locmaxy, locmaxz);
        double theta = _ParameterDensity->GetXaxis()->GetBinCenter(locmaxx - 1);
        double rho = _ParameterDensity->GetYaxis()->GetBinCenter(locmaxy - 1);
        _Lines.push_back(std::make_pair(theta, rho));
        _ParameterDensity->SetBinContent(binmax, 0);
    }
}

void HoughTransform::PointTransform(point2d_t point) {
    unsigned int nAngleSteps = TMath::Nint((TMath::Pi() * 1) / _AngleResolution);
    point2d_t origin = std::make_pair(0., 0.);
    for (size_t i = 0; i < nAngleSteps; i++) {
        double currentAngle = _AngleResolution * (double)i;
        double a = -TMath::Tan(currentAngle);
        double c = -(a * point.first) - point.second;
        line2d_t line = std::make_tuple(a, 1, c);
        double currentDistance = DistanceFromPointToLine(origin, line);
        _LineParameterSpace.emplace_back(currentAngle, currentDistance);
    }
}

double HoughTransform::DistanceFromPointToLine(point2d_t point, line2d_t line) {
    double a = std::get<0>(line);
    double b = std::get<1>(line);
    double c = std::get<2>(line);
    double x0 = point.first;
    double y0 = point.second;

    return TMath::Abs(a*x0 + b*y0 + c) / TMath::Sqrt(a*a + b*b);
}

void HoughTransform::Plot() {
    gStyle->SetOptStat(0);

    auto* x = new double[_NHits];
    auto* y = new double[_NHits];
    size_t i = 0;
    for (auto & _Hit : _Hits) {
        *(x + i) = _Hit.first;
        *(y + i) = _Hit.second;
        i++;
    }
    auto* graph_image = new TGraph(_NHits, x, y);


    std::vector<TGraph*> graphs_lines;
    auto* X = new double[2];
    *(X + 0) = 1;
    *(X + 1) = 5;
    for (size_t k = 0; k < _NLines; k++) {
        double rho = _Lines.at(k).second;
        double theta = _Lines.at(k).first;
        std::cout << "rho: " << rho << std::endl;
        std::cout << "theta: " << (theta / TMath::Pi()) * 180. << std::endl;
        auto* Y = new double[2];
        *(Y + 0) = (rho + TMath::Cos(theta) * (*(X + 0))) / TMath::Sin(theta);
        *(Y + 1) = (rho + TMath::Cos(theta) * (*(X + 1))) / TMath::Sin(theta);

        TGraph* graph_lines = new TGraph(2, X, Y);
        graphs_lines.push_back(graph_lines);
    }

    auto* rho = new double[_LineParameterSpace.size()];
    auto* theta = new double[_LineParameterSpace.size()];
    i = 0;
    for (auto & _Line : _LineParameterSpace) {
        *(rho + i) = _Line.second;
        *(theta + i) = _Line.first;
        i++;
    }
    auto* graph_HoughLines = new TGraph(_LineParameterSpace.size(), theta, rho);

    auto* graph_HoughPoints = new TGraph();
    for (size_t k = 0; k < _NLines; k++) {
        graph_HoughPoints->SetPoint(k, _Lines.at(k).first, _Lines.at(k).second);
    }

    auto* canvas = new TCanvas("canvas", "Canvas", 1200, 600);
    canvas->Divide(2, 1);

    canvas->cd(1);
    graph_image->SetMarkerColor(kRed);
    graph_image->SetMarkerStyle(5);
    graph_image->SetMarkerSize(1);
    TMultiGraph* mgr = new TMultiGraph();
    mgr->Add(graph_image, "P");
    for (size_t k = 0; k < _NLines; k++) {
        mgr->Add(graphs_lines.at(k));
    }
    mgr->Draw("APL");
    mgr->SetTitle("");
    mgr->GetXaxis()->SetTitle("x");
    mgr->GetXaxis()->CenterTitle();
    mgr->GetYaxis()->SetTitle("y");
    mgr->GetYaxis()->CenterTitle();
    gPad->Modified();
    gPad->Update();


    canvas->cd(2);
    graph_HoughPoints->SetMarkerStyle(20);
    graph_HoughPoints->SetMarkerSize(1);
    graph_HoughPoints->SetMarkerColor(kRed);
    TMultiGraph* mgr2 = new TMultiGraph();
    mgr2->Add(graph_HoughLines);
    mgr2->Add(graph_HoughPoints, "P");
    mgr2->SetTitle("");
    mgr2->GetXaxis()->SetTitle("Slope (radian)");
    mgr2->GetXaxis()->CenterTitle();
    mgr2->GetYaxis()->SetTitle("Y-intersection");
    mgr2->GetYaxis()->CenterTitle();
    mgr2->Draw("AP");

    canvas->SaveAs("HoughTransform.pdf");
}

void HoughTransform::SetHoughThreshold(size_t nlines) {
    _NLines = nlines;
}
